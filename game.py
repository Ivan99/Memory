import pygame
import random

SIVA = (200, 200, 200)
BIJELA = (255, 255, 255)
CRNA = (0, 0, 0)

WIDTH = 1024
HEIGHT = 768
size = (WIDTH, HEIGHT)

screen = pygame.display.set_mode(size)
pygame.display.set_caption("Memory igra - Made By: Ivan Zivkovic i Leonardo Markotic")
clock = pygame.time.Clock()

imenaslika = ["naslovna.jpg", "pozadina.jpg", "slika1.jpg", "slika2.jpg", "slika3.jpg", "slika4.jpg", "slika5.jpg", "slika6.jpg", "slika7.jpg", "slika8.jpg"]

naslovna = pygame.image.load(imenaslika[0])
pozadina = pygame.image.load(imenaslika[1])
slika1 = pygame.image.load(imenaslika[2])
slika2 = pygame.image.load(imenaslika[3])
slika3 = pygame.image.load(imenaslika[4])
slika4 = pygame.image.load(imenaslika[5])
slika5 = pygame.image.load(imenaslika[6])
slika6 = pygame.image.load(imenaslika[7])
slika7 = pygame.image.load(imenaslika[8])
slika8 = pygame.image.load(imenaslika[9])

listaslika = [pozadina, slika1, slika2, slika3, slika4, slika5, slika6, slika7, slika8]
listakarata = []
otvorenekarte = []

oKarta1 = None
oKarta2 = None

def PosloziSlike(cards):
	# 1-3
	img = cards[0][0]
	if cards[0][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[0][1] = screen.blit(img, (60.0, 45.0))

	img = cards[1][0]
	if cards[1][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[1][1] = screen.blit(img, (214.0, 45.0))

	img = cards[2][0]
	if cards[2][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[2][1] = screen.blit(img, (368.0, 45.0))

	img = cards[3][0]
	if cards[3][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[3][1] = screen.blit(img, (522.0, 45.0))

	# 4 - 6
	img = cards[4][0]
	if cards[4][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[4][1] = screen.blit(img, (60.0, 199.0))

	img = cards[5][0]
	if cards[5][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[5][1] = screen.blit(img, (214.0, 199.0))

	img = cards[6][0]
	if cards[6][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[6][1] = screen.blit(img, (368.0, 199.0))

	img = cards[7][0]
	if cards[7][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[7][1] = screen.blit(img, (522.0, 199.0))

	# 7 - 9
	img = cards[8][0]
	if cards[8][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[8][1] = screen.blit(img, (60.0, 353.0))

	img = cards[9][0]
	if cards[9][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[9][1] = screen.blit(img, (214.0, 353.0))

	img = cards[10][0]
	if cards[10][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[10][1] = screen.blit(img, (368.0, 353.0))

	img = cards[11][0]
	if cards[11][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[11][1] = screen.blit(img, (522.0, 353.0))

	# 10 - 13
	img = cards[12][0]
	if cards[12][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[12][1] = screen.blit(img, (60.0, 507.0))

	img = cards[13][0]
	if cards[13][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[13][1] = screen.blit(img, (214.0, 507.0))

	img = cards[14][0]
	if cards[14][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[14][1] = screen.blit(img, (368.0, 507.0))

	img = cards[15][0]
	if cards[15][2] == False:
		img = pygame.transform.scale(listaslika[0], (150, 150))
	cards[15][1] = screen.blit(img, (522.0, 507.0))	


kraj = False
stanje = "DobroDosli"
igracevRez = 0
cekanje = 450
pygame.font.init()

while not kraj:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			kraj = True
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_SPACE:
				if stanje == "KrajIgre" or stanje == "DobroDosli":
					stanje = "PostaviKarte"
					igracevRez = 0
			elif event.key == pygame.K_RETURN:
				if stanje == "Igra":
					stanje = "KrajIgre"
		elif event.type == pygame.MOUSEBUTTONDOWN:
			if stanje == "Igra":
				for k in listakarata:
					if k[1].collidepoint(event.pos):
						k[2] = True
						if oKarta1 == None:
							oKarta1 = k[3]
						elif oKarta2 == None:
							oKarta2 = k[3]

	if stanje == "DobroDosli":
		screen.fill(SIVA)
		naslovna = pygame.transform.scale(naslovna, (WIDTH, HEIGHT))
		screen.blit(naslovna, (0, 0))

		font = pygame.font.SysFont("Arial", 50, True)
		render = font.render("SPACE za pocetak!", True, BIJELA)
		screen.blit(render, ((WIDTH / 3.8, 100)))

		font = pygame.font.SysFont("Arial", 20, True)
		render = font.render("Made by: Ivan Zivkovic i Leonardo Markotic", True, SIVA)
		screen.blit(render, ((WIDTH / 2.28, HEIGHT - 25)))

	elif stanje == "PostaviKarte":
		screen.fill(SIVA)
		lista1 = []
		lista2 = []
		i = 0
		for slika in listaslika:
			if i > 0:
				s = pygame.transform.scale(slika, (150, 150))
				lista1.append([s, s.get_rect(), False, imenaslika[i]])
			i += 1
		random.shuffle(lista1)

		lista2 = []
		for slika in lista1:
			lista2.append([slika[0], slika[0].get_rect(), False, slika[3]])
		listakarata = lista1 + lista2
		random.shuffle(listakarata)
		stanje = "Igra"

	elif stanje == "Igra":	
		screen.fill(SIVA)
		font = pygame.font.SysFont("Arial", 22, True)
		text = "Score: " + str(igracevRez)
		render = font.render(text, True, CRNA)
		screen.blit(render, (10.0, 10.0))
		PosloziSlike(listakarata)

		if oKarta1 != None:
			if oKarta2 != None:
				cekanje = 450
				stanje = "Cekanje"

	elif stanje == "ProvjeriKarte":
		screen.fill(SIVA)
		font = pygame.font.SysFont("Arial", 22, True)
		text = "Score: " + str(igracevRez)
		render = font.render(text, True, CRNA)
		screen.blit(render, (10.0, 10.0))
		PosloziSlike(listakarata)

		if oKarta1 != oKarta2:
			for card in listakarata:
				if card[3] not in otvorenekarte:
					card[2] = False
		else:
			otvorenekarte.append(oKarta1)
			otvorenekarte.append(oKarta2)

		oKarta1 = None
		oKarta2 = None

		if len(otvorenekarte) == len(listakarata):
			igracevRez = igracevRez + 1
			stanje = "PostaviKarte"
			otvorenekarte = []
		else:
			stanje = "Igra"

	elif stanje == "Cekanje":
		screen.fill(SIVA)
		font = pygame.font.SysFont("Arial", 22, True)
		text = "Score: " + str(igracevRez)
		render = font.render(text, True, CRNA)
		screen.blit(render, (10.0, 10.0))	
		PosloziSlike(listakarata)

		cekanje -= clock.get_time()
		if cekanje <= 0:
			stanje = "ProvjeriKarte"
			cekanje = 450
			
	elif stanje == "KrajIgre":
		screen.fill(SIVA)
		naslovna = pygame.transform.scale(naslovna, (WIDTH, HEIGHT))
		screen.blit(naslovna, (0, 0))

		font = pygame.font.SysFont("Arial", 50, True)
		text = "Rezultat je: " + str(igracevRez)
		render = font.render(text, True, BIJELA)
		screen.blit(render, ((WIDTH / 4.5, 100)))

		font = pygame.font.SysFont("Arial", 20, True)
		render = font.render("Made by: Ivan Zivkovic i Leonardo Markotic", True, SIVA)
		screen.blit(render, ((WIDTH / 2.28, HEIGHT - 25)))

	pygame.display.flip()
	clock.tick(60)